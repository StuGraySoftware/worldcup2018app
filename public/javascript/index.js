/* Local Json Call */
// function jsonCall(requestURL){
//     var request = new XMLHttpRequest();
//     request.open('GET', '../../data/mainInfo.json');
//     request.responseType = 'json';
//     request.send();
//     request.onload = function() {
//         return(request.response);
//     }
// }
// var test;

// async function f() {
//   return 1;
// }

// f().then(alert);

// function jsonCall(requestURL) {
//   fetch(requestURL)
//   .then(function(response) {
//     response.json().then(function(data) {
//       test = data;
//       console.log(test);
//     });
//   })
//   .catch(function(err) {
//     console.log('error');
//   })
//     // var request = new XMLHttpRequest();
//     // request.open('GET', requestURL, true);
//     // request.onload = function() {
//     //   if (request.status >= 200 && request.status < 400) {
//     //     var data = JSON.parse(request.responseText);
//     //     console.log('data');
//     //     return data;
//     //   }
//     // };
//     // request.onerror = function() {
//     //   console.log('Error');
//     // };
//     // request.send();
// }

// var worldCupData = jsonCall('./data/mainInfo.json');

// console.log(test)

// async function jsonCall(requestURL) {
//   let response = await fetch(requestURL);
//   let user = await response.json();
//   await new Promise((resolve, reject) => setTimeout(resolve, 3000));
//   return user;
// }

// var test = jsonCall('./data/mainInfo.json');
// console.log(test);

// function doubleAfter2Seconds(x) {
//   return new Promise(resolve => {
//     setTimeout(() => {
//       resolve(x * 2);
//     }, 2000);
//   });
// }

async function addAsync(x) {
  // const a = await doubleAfter2Seconds(10);
  // const b = await doubleAfter2Seconds(20);
  // const c = await doubleAfter2Seconds(30);
  return x;
}


addAsync(10).then((sum) => {
  console.log(sum);
});